package mickael.cianamea.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        final WineDbHelper wineDbHelper = new WineDbHelper(this);
        if (wineDbHelper.fetchAllWines().getCount() == 0){
            wineDbHelper.populate();
        }
        Cursor cursor = wineDbHelper.fetchAllWines();
        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor,  new String[] {
                WineDbHelper.COLUMN_NAME,
                WineDbHelper.COLUMN_WINE_REGION},
                new int[]{android.R.id.text1, android.R.id.text2},0);

        final ListView listWine = (ListView) findViewById(R.id.listWineView);
        listWine.setAdapter(adapter);

        listWine.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SQLiteDatabase db = wineDbHelper.getReadableDatabase();
                String id1 = "_id="+ id;
                Wine wine1 = null;
                Cursor cursor = db.query(wineDbHelper.TABLE_NAME, null, id1, null, null ,null, null);
                if (cursor != null){
                    wine1 = wineDbHelper.cursorToWine(cursor);
                }
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("wine", wine1);
                startActivity(intent);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Wine wineAdd = new Wine("titre", "region", "localisation", "cimat", "surface");
               wineDbHelper.addWine(wineAdd);
               adapter.swapCursor(wineDbHelper.fetchAllWines());
               Toast.makeText(MainActivity.this, "Vin ajouté", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
