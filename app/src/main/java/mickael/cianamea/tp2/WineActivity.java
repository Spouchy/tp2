package mickael.cianamea.tp2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class WineActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        final WineDbHelper wineDbHelper = new WineDbHelper(this);
        final Wine wine1 = (Wine) getIntent().getParcelableExtra("wine");

        ((EditText) findViewById(R.id.wineName)).setText(wine1.getTitle());
        ((EditText) findViewById(R.id.editWineRegion)).setText(wine1.getRegion());
        ((EditText) findViewById(R.id.editClimate)).setText(wine1.getClimate());
        ((EditText) findViewById(R.id.editLoc)).setText(wine1.getLocalization());
        ((EditText) findViewById(R.id.editPlantedArea)).setText(wine1.getPlantedArea());

        final Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Wine upDateWine = new Wine();
                upDateWine.setId(wine1.getId());
                upDateWine.setTitle(((EditText) findViewById(R.id.wineName)).getText().toString());
                upDateWine.setRegion(((EditText) findViewById(R.id.editWineRegion)).getText().toString());
                upDateWine.setClimate(((EditText) findViewById(R.id.editClimate)).getText().toString());
                upDateWine.setLocalization(((EditText) findViewById(R.id.editLoc)).getText().toString());
                upDateWine.setPlantedArea(((EditText) findViewById(R.id.editPlantedArea)).getText().toString());
                wineDbHelper.updateWine(upDateWine);
            }
        });

    }
}
